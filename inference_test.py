from nns.keras_metrics_from_logits import precision, recall, binary_accuracy
import pandas as pd
from pathlib import Path
import h5py
from inference.InferenceModel import InferenceModel
from db_building.TrainingRead import Read
import matplotlib.pyplot as plt
import numpy as np
import time

COMPLEMENT_DICT = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C'}

def main():
    stride = 250
    counter = 0
    # Used for debugging
    num_of_reads_to_check = 683
    kmers_in_neg = []
    kmers_in_pos = []
    mod = InferenceModel('/lustre/BIF/nobackup/noord087/mscthesis/baseless/baseless_250_hyperparam/e_coli_compiled.tar')
    ground_truth = pd.read_csv('/lustre/BIF/nobackup/noord087/HoiCarlos/16Sreads_mockcommunity/ground_truth_with_read_id_and_perc_id.csv')
    start = time.time()
    for file in Path('/home/noord087/lustre_link/HoiCarlos/16Sreads_mockcommunity/demultiplexed_reads/files_for_initial_training/test').iterdir():
        file_info = ground_truth[ground_truth['file name'] == file.name]
        read_is_target_species = 'Escherichia coli' in str(file_info['species'])
        print(f"Target in read: {read_is_target_species}")
        with h5py.File(file, 'r') as f:
            # guppy_basecall = f['Analyses/Basecall_1D_000/BaseCalled_template/Fastq'][()].decode('ascii')
            # seq_guppy = guppy_basecall.splitlines()[1]
            raw_read_split_reads = Read(f, 'median').get_split_raw_read(250,
                                                                        stride)
        # # Get their reverse complements
        # rev_com_e_coli_kmers = [
        #     ''.join([COMPLEMENT_DICT[n] for n in kmer][::-1]) for kmer in
        #     mod.kmers]
        # actual_kmers = [kmer for (kmer, rev_kmer) in
        #                 zip(mod.kmers, rev_com_e_coli_kmers)
        #                 if kmer in seq_guppy or rev_kmer in seq_guppy
        #                 ]
        #
        # print(f'{actual_kmers=}')

        total_kmers_found = [kmer for kmer in mod.kmers if
                             mod.predict(raw_read_split_reads, kmer)]
        # print(f'{total_kmers_found=}')
        print(f'Total kmers: {len(total_kmers_found)}')
        print(counter/num_of_reads_to_check)
        print()
        counter += 1
        if read_is_target_species:
            kmers_in_pos.append(len(total_kmers_found))
        else:
            kmers_in_neg.append(len(total_kmers_found))

        if counter > num_of_reads_to_check:
            break
    print(f'Total time for {num_of_reads_to_check} reads: {time.time() - start} seconds')
    bins = np.arange(0, 22, 1)
    plt.hist(kmers_in_neg, bins=bins, alpha=0.5, label='Neg')
    plt.hist(kmers_in_pos, bins=bins, alpha=0.5, label='Pos')
    plt.legend()
    plt.show()


def main_scan_for_kmers_in_basecalls():
    # 24 kmers in E coli for which we trained models
    e_coli_kmers = ['AAACTTCC', 'ACGACTTC', 'ACTAGCGA', 'AGATGGAT', 'AGGAAGGG',
                    'AGTTTTAA', 'ATCCTCTC', 'CAAAGGAG', 'CAACACGA', 'CAAGCGGA',
                    'CCATTGTA', 'CCCGCAGA', 'CCGTACTC', 'CCTTCGCC', 'CTCTCAGA',
                    'CTTCATAC', 'CTTCCTCC', 'CTTCTTCC', 'CTTGCGGC', 'GCATCGAA',
                    'GCTTCCCA', 'GGAGTCGA', 'GGTATCTA', 'TAAGTCGA']
    # Get their reverse complements
    rev_com_e_coli_kmers = [
        ''.join([COMPLEMENT_DICT[n] for n in kmer][::-1]) for kmer in
        e_coli_kmers]

    ground_truth = pd.read_csv('/lustre/BIF/nobackup/noord087/HoiCarlos/16Sreads_mockcommunity/ground_truth_with_read_id_and_perc_id.csv')
    for file in Path('/home/noord087/lustre_link/HoiCarlos/16Sreads_mockcommunity/demultiplexed_reads/files_for_initial_training/test').iterdir():
        file_info = ground_truth[ground_truth['file name'] == file.name]
        print(f"Target in read: {'Escherichia coli' in str(file_info['species'])}")
        print(file.name)
        with h5py.File(file, 'r') as f:
            guppy_basecall = f['Analyses/Basecall_1D_000/BaseCalled_template/Fastq'][()].decode('ascii')
            seq_guppy = guppy_basecall.splitlines()[1]
            # tombo_events = f['Analyses/RawGenomeCorrected_000/BaseCalled_template/Events']['base'].astype(str)
            # seq_tombo = ''.join(tombo_events)

        kmers_found = sum(kmer in seq_guppy for kmer in e_coli_kmers)
        rev_kmers_found = sum(kmer in seq_guppy
                              for kmer in rev_com_e_coli_kmers)

        print(max(kmers_found, rev_kmers_found), 'of 24 found in target')
        print()


if __name__ == '__main__':
    main()
