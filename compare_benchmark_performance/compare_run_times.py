import argparse
from datetime import timedelta
from pathlib import Path
from multiprocessing import Pool
from itertools import chain

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.dates import date2num
import seaborn as sns


def parse_benchmark_file(file_path):
    """Extract runtime from a benchmark file output by snakemake

    :param file_path: path to benchmark file
    :type file_path: Path
    :return: tuple of toolname, target species, fold no,
    walltime and cpu time as timedelta object
    """
    print(f'Processing {file_path}')
    fold = file_path.stem[-1]
    target_species = file_path.parts[-2].replace('_', ' ')
    tool = file_path.parts[-3]
    if tool == 'deepnano':
        if 'basecall' in file_path.stem:
            tool += ' basecall'
        else:
            tool += ' minimap'
    elif tool == 'uncalled':
        if 'index' in file_path.stem:
            tool += ' index'
            fold = None
        else:
            tool += ' map'
    df = pd.read_csv(file_path, sep='\t')
    walltime = timedelta(seconds=float(df.s))
    cpu_time = timedelta(seconds=float(df.cpu_time))

    return tool, target_species, fold, walltime, cpu_time


def main(args):
    # For debugging:
    pd.options.display.width = 0

    # Runtime parsing
    deepnano_basecall_benchmark_files = args.benchmark_path.glob(
        '**/deepnano_basecall_benchmark_fold?.txt')
    deepnano_minimap_benchmark_files = args.benchmark_path.glob(
        '**/minimap_deepnano_benchmark_fold?.txt')
    guppy_benchmark_files = args.benchmark_path.glob(
        '**/guppy_benchmark_fold?.txt')
    uncalled_map_benchmark_files = args.benchmark_path.glob(
        '**/uncalled_map_benchmark_fold?.txt')

    uncalled_index_benchmark_files = args.benchmark_path.glob(
        '**/uncalled_index_benchmark.txt')

    with Pool(args.threads) as p:
        all_files = chain(deepnano_basecall_benchmark_files,
                          deepnano_minimap_benchmark_files,
                          guppy_benchmark_files,
                          uncalled_map_benchmark_files,
                          uncalled_index_benchmark_files)

        all_benchmarks = p.map(parse_benchmark_file, all_files)

    assert all_benchmarks, 'No files found'

    df = pd.DataFrame.from_records(all_benchmarks, columns=['tool',
                                                            'species',
                                                            'fold',
                                                            'walltime',
                                                            'cpu_time'])

    # Set species in alphabetical order
    all_species = df.species.unique()
    all_species.sort()
    # Generate time for matplotlib
    df['matplotlib_time'] = date2num(df.cpu_time)
    df.to_csv(args.out_dir / 'deepnano_uncalled_guppy_runtimes.csv',
              index=False)

    # Provisional plot:
    sns.catplot(x='species', y='matplotlib_time', data=df, ci='sd',
                hue='tool', order=all_species, kind='bar', legend_out=False,
                aspect=2, log=True)
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.savefig(args.out_dir / 'runtime_benchmark.png')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="""Plot runtime of
     benchmarked algorithms""")
    parser.add_argument('--benchmark-path',
                        help='Path to folder that contains all benchmarks '
                             'output by the snakemake workflow. '
                             'Should contain a folder called output_16s_files',
                        required=True, type=Path)
    parser.add_argument('--out-dir',
                        help='Directory in which to save the output figure',
                        required=True, type=Path)
    parser.add_argument('--threads',
                        help='Number of threads to run in parallel',
                        required=False, type=int, default=30)

    args = parser.parse_args()
    main(args)
