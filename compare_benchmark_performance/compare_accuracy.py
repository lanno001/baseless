import argparse
from pathlib import Path
from itertools import chain
from multiprocessing import Pool
import pickle

import pandas as pd
from sklearn.metrics import (confusion_matrix, ConfusionMatrixDisplay,
                             f1_score,  accuracy_score)
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

SPECIES_TO_ID = {
    'acinetobacter baumannii': 'NZ_CP053098',
    # Two names for the same species here
    'actinomyces odontolyticus': 'NZ_DS26458',
    'schaalia odontolytica': 'NZ_DS26458',
    'bacillus cereus': 'AE017194',
    'bacteroides vulgatus': 'CP000139',
    'clostridium beijerinckii': 'CP000721',
    'deinococcus radiodurans': 'AE000513',
    'enterococcus faecalis': 'CP025020',
    'escherichia coli': 'U00096',
    'helicobacter pylori': 'AE000511',
    'lactobacillus gasseri': 'CP000413',
    'listeria monocytogenes': 'AL591824',
    'neisseria meningitides': 'AE002098',
    'porphyromonas gingivalis': 'AP009380',
    # Never found in ground truth but included for completion
    'propionibacterium acnes': 'AE017283',
    'pseudomonas aeruginosa': 'AE004091',
    'rhodobacter sphaeroides': 'CP00014',
    'staphylococcus aureus': 'CP000730',
    'staphylococcus epidermidis': 'AE015929',
    'streptococcus agalactiae': 'AE009948',
    'streptococcus mutans': 'AE014133',
    'streptococcus pneumoniae': 'AE005672',
}


def parse_sequencing_summary(file_path, ground_truth):
    """Parse a sequencing_summary.txt output by guppy

    :param file_path: Path to sequencing_summary.txt. Should be in the form of
    **/guppy/{target}/fold{ix}/sequencing_summary.txt
    :type file_path: Path
    :param ground_truth: Dataframe that contains ground truth, can be read from
     csv that is output by tools.set_ground_truths_of_reads.py
    :type ground_truth: pd.DataFrame
    :return: tuple with tool, target_species, fold, accuracy,
    f1 and confusion matrix
    """
    print(f'Processing {file_path}')
    # Extract some properties from the file path
    fold = file_path.parts[-2][-1]
    target_species = file_path.parts[-3].replace('_', ' ')
    target_id = SPECIES_TO_ID[target_species]
    tool = file_path.parts[-4]
    assert tool == 'guppy'

    df_guppy = pd.read_csv(file_path, sep='\t')
    df_guppy.rename(columns={'read_id': 'read id'}, inplace=True)
    df = df_guppy.merge(ground_truth, on='read id')

    # Set truth to 1 if ground truth species is found
    y_true = df['species id'].apply(lambda x:
                                    1 if x.find(target_id) == 0
                                    else 0)

    y_pred = df['alignment_genome'].apply(lambda x: 0 if x == '*' else 1)
    f1 = f1_score(y_true, y_pred)
    accuracy = accuracy_score(y_true, y_pred)
    cm = confusion_matrix(y_true, y_pred)
    return tool, target_species, fold, accuracy, f1, cm


def parse_paf(paf_path, ground_truth):
    """Parse single paf output of uncalled or minimap2

    :param paf_path: path to .paf file output. Should be mapped
    to single species reference genome.
    If mapped to no species, the target sequence name should be '*'
    :param ground_truth: Dataframe that contains ground truth, can be read from
     csv that is output by tools.set_ground_truths_of_reads.py
    :type ground_truth: pd.DataFrame
    :return: tuple with tool, target_species, fold, accuracy, f1
    """
    headers = [
        "read id",
        "Query sequence length",
        "Query start (0-based; BED-like; closed)",
        "Query end (0-based; BED-like; open)",
        "Relative strand: + or -",
        "Target sequence name",
        "Target sequence length",
        "Target start on original strand (0-based)",
        "Target end on original strand (0-based)",
        "Number of residue matches",
        "Alignment block length",
        "Mapping quality (0-255; 255 for missing)"
    ]

    print(f'Processing {paf_path}')
    fold = paf_path.stem[-1]
    target_species = paf_path.parts[-2].replace('_', ' ')
    target_id = SPECIES_TO_ID[target_species]
    tool = paf_path.parts[-3]

    uncalled_paf = pd.read_csv(paf_path, sep='\t', names=headers,
                               usecols=range(12))
    merged_df = uncalled_paf.merge(ground_truth, on='read id')
    # Set truth to 1 if ground truth species is found
    y_true = merged_df['species id'].apply(lambda x:
                                           1 if x.find(target_id) == 0
                                           else 0)
    # Mapping to a * means mapping to no species
    y_pred = merged_df['Target sequence name'].apply(
        lambda x: 0 if x == '*' else 1)
    f1 = f1_score(y_true, y_pred)
    accuracy = accuracy_score(y_true, y_pred)
    cm = confusion_matrix(y_true, y_pred)
    return tool, target_species, fold, accuracy, f1, cm


def parse_squigglenet_output(in_dir, ground_truth):
    """Parse directory with inference txt files output by squigglenet

    :param in_dir: path to directory with batch_?.txt
    :type in_dir: Path
    :param ground_truth: dataframe of ground truth
    :type ground_truth: pd.Dataframe
    :return: tuple with tool, target_species, fold, accuracy, f1
    """
    fold = in_dir.parts[-2][-1]
    target_species = in_dir.parts[-3].replace('_', ' ')
    target_id = SPECIES_TO_ID[target_species]
    tool = in_dir.parts[-4]
    assert tool == 'squigglenet'

    predictions = []
    for file in in_dir.iterdir():
        with file.open() as f:
            temp = f.readlines()
            temp = [i.split() for i in temp]
            predictions.extend(temp)

    df = pd.DataFrame.from_records(predictions, columns=['read id', 'y pred'])

    merged_df = df.merge(ground_truth, on='read id')
    y_true = merged_df['species id'].apply(lambda x:
                                           1 if x.find(target_id) == 0
                                           else 0)
    y_pred = merged_df['y pred'].astype(int)
    # Flip predictions because squigglenet be like that
    y_flipped = 1 - y_pred
    f1 = f1_score(y_true, y_flipped)
    accuracy = accuracy_score(y_true, y_flipped)
    cm = confusion_matrix(y_true, y_flipped)
    return tool, target_species, fold, accuracy, f1, cm


def calculate_accuracy_from_output(args):
    """Function that can be called from main. Takes raw tool output data and
    calculates accuracy, f1 score and confusion matrix for each tool, species
    and fold. Returns this data as a dataframe and also saves it"""
    ground_truth = pd.read_csv(args.ground_truth)
    # Find accuracy files
    deepnano_files = args.benchmark_path.glob(
        "deepnano/*/minimap2_deepnano_fold*.paf")
    uncalled_files = args.benchmark_path.glob(
        "uncalled/*/uncalled_out_fold*.paf")
    guppy_files = args.benchmark_path.glob(
        "guppy/*/fold?/sequencing_summary.txt")
    squigglenet_files = args.benchmark_path.glob(
        "squigglenet/*/fold?/inference")
    # Prepare arguments to parse to the functions in the pool
    args_uncalled_deepnano = [[file, ground_truth] for file
                              in chain(deepnano_files, uncalled_files)]
    args_guppy = [[file, ground_truth] for file in guppy_files]
    args_squigglenet = [[folder, ground_truth]
                        for folder in squigglenet_files]
    with Pool(args.threads) as p:
        squigglenet_results = p.starmap(parse_squigglenet_output,
                                        args_squigglenet)
        uncalled_deepnano_results = p.starmap(parse_paf,
                                              args_uncalled_deepnano)
        guppy_results = p.starmap(parse_sequencing_summary, args_guppy)
    all_records = (uncalled_deepnano_results + guppy_results
                   + squigglenet_results)
    assert all_records, ('No files found: make sure the input folder '
                         'contains directories called "uncalled",'
                         ' "guppy" and "deepnano"')
    df = pd.DataFrame.from_records(all_records,
                                   columns=['tool', 'species',
                                            'fold', 'accuracy', 'f1',
                                            'confusion matrix'])
    # Create confusion matrices
    cm_series = df.groupby(['tool', 'species'])['confusion matrix'].aggregate(
        np.sum)
    with open(args.out_dir / f'confusion_matrices.pickle', 'wb') as f:
        pickle.dump(cm_series, f)

    # Confusion matrix out directory
    cm_out_dir = args.out_dir / 'confusion_matrices'
    cm_out_dir.mkdir(exist_ok=True)
    for (tool, species), cm in cm_series.iteritems():
        ConfusionMatrixDisplay(cm).plot()
        plt.savefig(cm_out_dir / f'{tool}_{species}.png')

    df.to_csv(args.out_dir / f'all_algorithms_performance.csv', index=False)
    return df


def main(args):
    # For debugging
    pd.options.display.width = 0

    if not args.input_performance_csv:
        df, filename = calculate_accuracy_from_output(args)
    else:
        df = pd.read_csv(args.input_performance_csv)

    # Set species in alphabetical order
    all_species = df.species.unique()
    all_species.sort()

    g = sns.catplot(x='species', y='accuracy', ci='sd', data=df, hue='tool',
                    kind='bar', order=all_species, legend_out=False, aspect=2)
    g.set_xticklabels(rotation=45, ha="right",
                      rotation_mode="anchor")
    # g.ax.set_title('5-fold CV benchmarked model accuracy')
    g.fig.suptitle('5-fold CV model accuracy per bacterial species')
    g.tight_layout()
    g.savefig(args.out_dir / 'all_algorithms_performance_accuracy.png')

    g = sns.catplot(x='species', y='f1', ci='sd', data=df, hue='tool',
                    kind='bar', order=all_species, legend_out=False, aspect=2)
    g.set_xticklabels(rotation=45, ha="right",
                      rotation_mode="anchor")
    g.ax.set_title(r'5-fold CV model $F_1$ score per bacterial species')
    g.tight_layout()
    g.savefig(args.out_dir / 'all_algorithms_performance_f1.png')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="""Plot accuracy of
        benchmarked algorithms. Provide only performance CSV and out-dir
        or all arguments except for input-performance-csv""")
    parser.add_argument('--benchmark-path',
                        help='Path to folder that contains all benchmarks '
                             'output by the snakemake workflow. '
                             'Should contain a folders called "uncalled",'
                             ' "guppy" and "deepnano"',
                        required=True, type=Path)
    parser.add_argument('--ground-truth',
                        help='Path to csv with ground truth labels. '
                             'It is output by set_ground_truths_of_reads.py',
                        required=True, type=Path)
    parser.add_argument('--input-performance-csv',
                        help='Path to csv that contains model performance as '
                             'output by earlier run of this script. '
                             'If provided, the script will not recalculate '
                             'all performance statistics manually but '
                             'read them from this csv.',
                        required=False, type=Path)
    parser.add_argument('--out-dir',
                        help='Directory in which to save the figures and'
                             ' csv with model performance',
                        required=True, type=Path)
    parser.add_argument('--threads',
                        help='Number of threads to run in parallel',
                        required=False, type=int, default=30)
    args = parser.parse_args()
    main(args)
