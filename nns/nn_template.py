import tensorflow as tf
from tensorflow.keras.preprocessing.sequence import pad_sequences
# from tensorflow.keras.metrics import Accuracy, Precision, Recall
import numpy as np
from helper_functions import clean_classifications


class NeuralNetwork(object):
    def __init__(self, **kwargs):

        # Ensure these attributes are in. Additional attributes may be defined
        self.target = kwargs['target']
        self.filter_width = kwargs['filter_width']
        self.hfw = (self.filter_width - 1) // 2  # half filter width
        self.cp_callback = kwargs['cp_callback']
        self.initialize(kwargs['weights'])
        self.history = {'loss':[], 'binary_accuracy': [], 'precision': [], 'recall': [],
                        'val_loss': [], 'val_binary_accuracy':[], 'val_precision': [], 'val_recall': []}

    def initialize(self, weights):
        """
        Initialize the network. If weights are provided, these should be loaded.
        """
        pass

    def train(self, x, y, x_val, y_val, eps_per_kmer_switch=100):
        """
        Train the network. x_val/y_val may be used for validation/early stopping mechanisms.
        """
        pass

    def predict(self, x, clean_signal=True, return_probs=False):
        # todo check for better GPU/TPU performance: https://www.tensorflow.org/guide/data_performance
        offset = 5
        ho = offset // 2
        lb, rb = self.hfw - ho, self.hfw + ho + 1
        idx = np.arange(self.filter_width, len(x) + offset, offset)
        x_batched = [x[si:ei] for si, ei in zip(idx-100, idx)]
        x_pad = pad_sequences(x_batched, padding='post', dtype='float32')
        posteriors = self.model.predict(x_pad)
        y_hat = posteriors > self.threshold
        y_out = np.zeros(len(x), dtype=int)
        for i, yh in enumerate(y_hat):
            y_out[lb + i * offset :rb + i * offset] = yh
        # todo include clean signal
        if return_probs:
            posteriors_out = np.zeros(len(x), dtype=float)
            for i, p in enumerate(posteriors):
                posteriors_out[lb + i * offset :rb + i * offset] = p
            return y_out, posteriors_out
        return y_out
