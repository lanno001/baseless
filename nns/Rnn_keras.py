import tensorflow as tf
from tensorflow.keras.preprocessing.sequence import pad_sequences
# from tensorflow.keras.metrics import Accuracy, Precision, Recall
import numpy as np
from helper_functions import clean_classifications
from tensorflow.keras import backend
from nns.keras_metrics_from_logits import precision, recall, binary_accuracy


class NeuralNetwork(object):
    def __init__(self, **kwargs):
        self.target = kwargs['target']
        self.layer_size = kwargs['layer_size']
        self.batch_size = kwargs['batch_size']
        self.num_layers = kwargs['num_layers']
        self.max_sequence_length = kwargs['max_sequence_length']
        self.cell_type = kwargs['cell_type']
        self.name_optimizer = kwargs['name_optimizer']
        self.learning_rate = kwargs['learning_rate']
        self.dropout_keep_prob = kwargs['dropout_keep_prob']
        self.threshold=kwargs['threshold']
        self.filter_width = kwargs['filter_width']
        self.hfw = (self.filter_width - 1) // 2  # half filter width
        self.callbacks = []
        self.inference_callbacks = []
        if kwargs['cp_callback']:
            self.callbacks.append(kwargs['cp_callback'])
        if kwargs['tb_callback']:
            self.callbacks.append(kwargs['tb_callback'])
            self.inference_callbacks.append(kwargs['tb_callback'])
        self.initialize(kwargs['weights'])
        self.history = {'loss':[], 'binary_accuracy': [], 'precision': [], 'recall': [],
                        'val_loss': [], 'val_binary_accuracy':[], 'val_precision': [], 'val_recall': []}

    def initialize(self, weights):
        rnn_layers = [tf.keras.layers.GRU(self.layer_size, return_sequences=True) for _ in range(self.num_layers - 1)] + [tf.keras.layers.GRU(self.layer_size)]
        self.model = tf.keras.Sequential(
            [tf.keras.layers.Masking(input_shape=(self.filter_width, 1))]
            + rnn_layers + [tf.keras.layers.Dense(1, activation=None)]
        )
        # if weights is not None:
        #     pass # todo
        self.model.compile(
            loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
            optimizer=tf.keras.optimizers.Adam(learning_rate=self.learning_rate, epsilon=1e-4),
            # metrics=['BinaryAccuracy', 'Precision', 'Recall'],
            metrics=[binary_accuracy, precision, recall],
            # metrics=[tf.keras.metrics.Precision,
            #          tf.keras.metrics.Recall,
            #          tf.keras.metrics.Accuracy
            #          ]
        )

    def train(self, x, y, x_val, y_val, eps_per_kmer_switch=100, quiet=False):
        x_pad = np.expand_dims(pad_sequences(x, padding='post', dtype='float32'), -1)
        x_val_pad = np.expand_dims(pad_sequences(x_val, padding='post', dtype='float32'), -1)
        tfd = tf.data.Dataset.from_tensor_slices((x_pad, y)).batch(self.batch_size).shuffle(x_pad.shape[0], reshuffle_each_iteration=True) # todo: is y.astype('float32') faster?
        self.model.fit(tfd, epochs=eps_per_kmer_switch,
                       callbacks=self.callbacks,
                       validation_data=(x_val_pad, y_val), validation_steps=10,
                       validation_freq=max(eps_per_kmer_switch, eps_per_kmer_switch // 10),
                       verbose=[2, 0][quiet])
        for hv in self.model.history.history: self.history[hv].extend(self.model.history.history[hv])

    def predict(self, x, clean_signal=True, return_probs=False):
        # todo check for better GPU/TPU performance: https://www.tensorflow.org/guide/data_performance
        offset = 5
        ho = offset // 2
        lb, rb = self.hfw - ho, self.hfw + ho + 1
        idx = np.arange(self.filter_width, len(x) + offset, offset)
        x_batched = [x[si:ei] for si, ei in zip(idx-self.filter_width, idx)]
        x_pad = pad_sequences(x_batched, padding='post', dtype='float32')
        posteriors = self.model.predict(x_pad, callbacks=self.inference_callbacks)
        y_hat = posteriors > self.threshold
        y_out = np.zeros(len(x), dtype=int)
        for i, yh in enumerate(y_hat):
            y_out[lb + i * offset :rb + i * offset] = yh
        # todo include clean signal
        if return_probs:
            posteriors_out = np.zeros(len(x), dtype=float)
            for i, p in enumerate(posteriors):
                posteriors_out[lb + i * offset :rb + i * offset] = p
            return y_out, posteriors_out
        return y_out
