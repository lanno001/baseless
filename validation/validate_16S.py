import os, sys, re
import pandas as pd
from os.path import basename
from shutil import copy
from sklearn.model_selection import StratifiedKFold
from jinja2 import Template
from datetime import datetime

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
baseless_location = os.path.realpath(f'{__location__}/..')
sys.path.append(baseless_location)

import numpy as np
from argparse_dicts import get_validate_parser
from low_requirement_helper_functions import parse_input_path, parse_output_path
from snakemake import snakemake
import yaml
import multiprocessing as mp


def parse_fast5_list(fast5_list, gt_df, out_queue):
    fast5_basename_list = [basename(f) for f in fast5_list]
    fast5_df = pd.DataFrame({'read_id': fast5_basename_list,
                             'fn': fast5_list,
                             'species': [gt_df.species_short.get(ff, 'unknown') for ff in fast5_basename_list]}
                            ).set_index('read_id')
    fast5_df.drop(fast5_df.query('species == "unknown"').index, axis=0, inplace=True)
    out_queue.put(fast5_df)


def write_test_reads(fast5_sub_df, nb_folds, test_read_dir, species_list):
    for nf in range(nb_folds):
        cur_fold_df = fast5_sub_df.query(f'fold_{nf} == False')
        for _, fn in cur_fold_df.fn.iteritems():
            for sp in species_list:
                copy(fn, f'{test_read_dir}{sp}/fold_{nf}/')


parser = get_validate_parser()
args = parser.parse_args()

with open(args.parameter_file, 'r') as pf: params = yaml.load(pf, Loader=yaml.FullLoader)
# primed_nn_dir = args.primed_nn_dir
# if primed_nn_dir[-1] != '/': primed_nn_dir += '/'

# --- generate folder structure ---
out_dir = parse_output_path(args.out_dir)
compiled_mod_dir = parse_output_path(args.out_dir + 'compiled_mods/')
logs_dir = parse_output_path(args.out_dir + 'logs/')

# --- Generate read index files for folds ---
gt_df = pd.read_csv(args.ground_truth_16s, header=0)
gt_df.columns = [cn.replace(' ', '_') for cn in gt_df.columns]
gt_df.set_index('file_name', inplace=True)

print(f'{datetime.now()}: loading fast5 file list...')
read_index_dir = parse_output_path(f'{args.out_dir}read_index_files')
fast5_list = np.array(parse_input_path(args.fast5_in, pattern='*.fast5'))
nb_fast5 = len(fast5_list)
ff_idx_list = np.array_split(np.arange(len(fast5_list)), args.cores)

print(f'{datetime.now()}: parsing fast5 file list...')
out_queue = mp.Queue()
ff_workers = [mp.Process(target=parse_fast5_list,
                         args=(fast5_list[ff_idx], gt_df, out_queue)) for ff_idx in ff_idx_list]
for worker in ff_workers:
    tst = worker.start()
df_list = []
while any(p.is_alive() for p in ff_workers):
    while not out_queue.empty():
        df_list.append(out_queue.get())
fast5_df = pd.concat(df_list)

print(f'{datetime.now()}: done')

# --- compile species list ---
species_list = []
with open(args.target_16S, 'r') as fh:
    for line in fh.readlines():
        if gen := re.search('(?<=>)[^,]+', line): species_list.append(gen.group(0))
species_list = list(set(species_list))

# --- make folders with test reads (inference consumes them, so separately for each species) ---

species_reads_list = list(fast5_df.species.unique())
for sp in species_list:
    if sp not in species_reads_list:
        raise ValueError(f'No reads for target species {sp} listed in ground truth file!')
test_read_dir = parse_output_path(f'{args.out_dir}test_reads/', clean=True)

if args.index_files:
    index_fn_list = sorted(os.listdir(args.index_files))
    for fi, fn in enumerate(index_fn_list):
        idx_df = pd.read_csv(f'{args.index_files}{fn}', index_col=0)
        idx_df.loc[:, 'fn'] = args.fast5_in + idx_df.index
        fast5_df.loc[:, f'fold_{fi}'] = idx_df.loc[fast5_df.index, 'fold']
        idx_df.to_csv(f'{read_index_dir}index_fold{fi}.csv')
        # copy(f'{args.index_files}{fn}', f'{read_index_dir}index_fold{fi}.csv')
        for sp in species_list:
            _ = parse_output_path(f'{test_read_dir}{sp}/fold_{fi}')
else:
    for fi, (train_num_idx, _) in enumerate(StratifiedKFold(n_splits=args.nb_folds,
                                                            shuffle=True).split(fast5_df.index,
                                                                                fast5_df.species)):
        for sp in species_list:
            _ = parse_output_path(f'{test_read_dir}{sp}/fold_{fi}')
        train_idx = fast5_df.index[train_num_idx]
        fast5_df.loc[:, f'fold_{fi}'] = False
        fast5_df.loc[train_idx, f'fold_{fi}'] = True
        fast5_df.to_csv(f'{read_index_dir}index_fold{fi}.csv',
                        columns=['fn', 'species', f'fold_{fi}'],
                        header=['fn', 'species', f'fold'])

print(f'{datetime.now()}: writing test read dirs...')
ff_idx_list = np.array_split(np.arange(len(fast5_df)), args.cores)
workers = [mp.Process(target=write_test_reads, args=(fast5_df.iloc[cidx, :], args.nb_folds, test_read_dir, species_list))
           for cidx in ff_idx_list]
for worker in workers:
    tst = worker.start()
for worker in workers:
    worker.join()

print(f'{datetime.now()}: done')

# --- compile snakefile and run ---
with open(f'{__location__}/validate_16S.sf', 'r') as fh: template_txt = fh.read()
sm_text = Template(template_txt).render(
    baseless_location=baseless_location,
    species_list=species_list,
    hdf_path=args.hdf_path,
    parameter_file=args.parameter_file,
    logs_dir=logs_dir,
    # primed_nn_dir=primed_nn_dir,
    compiled_mod_dir=compiled_mod_dir,
    target_16S_fasta=os.path.abspath(args.target_16S),
    nb_folds=args.nb_folds,
    nn_dir=args.nn_dir,  # premade nns
    additional_nn_dir=parse_output_path(out_dir + 'additional_nns/'),  # directory for nns to be constructed on the fly
    inference_out_dir=parse_output_path(args.out_dir + 'inference_out/'),
    inference_summary_dir=parse_output_path(args.out_dir + 'inference_summaries/'),
    benchmark_dir=parse_output_path(args.out_dir + 'inference_benchmark/'),
    target_16S_dir=parse_output_path(args.out_dir + 'target_fastas/'),
    reads_dir=args.fast5_in,
    test_read_dir=test_read_dir,
    read_index_dir=read_index_dir,
    train_required=args.train_required
)

sf_fn = f'{args.out_dir}validate_16S_pipeline.sf'
with open(sf_fn, 'w') as fh: fh.write(sm_text)
snakemake(sf_fn, cores=args.cores, verbose=False, keepgoing=True, dryrun=args.dryrun)
