import numpy as np
import pandas as pd
import matplotlib
# matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import statsmodels.api as sm

data = pd.read_csv('/home/carlos/local_data/train_performance.txt',
                   sep='\t')
data['example'] = np.expand_dims(np.linspace(10,data.shape[0]*10, num=data.shape[0]), axis=-1)
data_loess = data.copy()
data_loess = data_loess.drop(['TNR', 'accuracy'], axis=1)
data_loess = data_loess.rename(columns={'TPR': 'precision'})

metrics = list(data_loess)
metrics.remove('example')


for metric in metrics:
    # data_loess[metric] = sm.nonparametric.lowess(endog=np.array(data[metric]),
    #                                        exog=np.array(data.example),
    #                                        frac=0.9)
    data_loess[metric] = data_loess[metric].rolling(10).mean()

data_loess = data_loess.melt(id_vars=['example'], var_name='metric')

sns.set_style('ticks')
fig, ax = plt.subplots()
fig.set_size_inches(11.7, 4.5)
sns.tsplot(time='example',
           unit = 'metric', condition='metric',
           value='value', data=data_loess,
           color={'precision': '#3F9C35',
                  'recall': '#005172'})
for it, xc in enumerate(range(320, data['example'].max().astype(int), 320)):
    ax.text(xc-210, 0.635, 'epoch %s' % int(it+1))
    ax.axvline(x=xc, linewidth=1,color='black')
# fig.show()
fig.savefig('train_performance_timeseries.png')