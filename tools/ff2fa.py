import argparse
import h5py
import os
import warnings
import re

parser = argparse.ArgumentParser(description="Extract fastq-file from fast5-file")
parser.add_argument('fast5_files', type=str, nargs="+",
                    help="fast5-files and/or directories of fast5-files that should be converted")
parser.add_argument('--output-directory', type=str, required=True,
                    help='Location where created fasta-files should be stored.')

args = parser.parse_args()
location = args.fast5_files

# Convert input argument into list of fast5 files, if it is not in that format
if not isinstance(location, list):
    location = [location]

all_files = []
for loc in location:
    if os.path.isdir(loc):
        if loc[-1] != '/':
            loc += '/'
        file_names = os.listdir(loc)
        files = [loc + f for f in file_names]
        all_files.extend(files)
    elif os.path.exists(loc):
        all_files.extend(loc)
    else:
        warnings.warn('Given location %s does not exist, skipping' % loc, RuntimeWarning)
if not len(all_files):
    ValueError('Input file location(s) did not exist or did not contain any files.')

# Correct and create output directory if needed
if args.output_directory[-1] != '/':
    args.output_directory += '/'
if not os.path.isdir(args.output_directory):
    os.makedirs(args.output_directory)

for file in all_files:
    with h5py.File(file, 'r') as ff:
        fq = ff['Analyses/Basecall_1D_000/BaseCalled_template/Fastq'][()].decode('ascii')
    fq_name = re.search('.+(?=fast5)', os.path.basename(file)).group(0)
    fq_name += 'fastq'
    with open(args.output_directory+fq_name, "w") as out_file:
        out_file.write(fq)
