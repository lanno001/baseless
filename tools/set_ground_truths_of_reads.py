import argparse
from pathlib import Path
import pandas as pd
import Bio.Blast.NCBIXML
import multiprocessing


def parse_blast_xml(file_path):
    """Given path to one blast xml output, extract information that
    will be used in ground truth table

    :param file_path: Path object to single file
    :type file_path: Path
    :return: tuple with file_name, read_id, species,
             percent identity of best hsp, alignment length of best hsp,
             species_id
    """
    with file_path.open() as f:
        blast = Bio.Blast.NCBIXML.read(f)

    file_name = file_path.name.replace('_0.xml', '.fast5')
    read_id = blast.query.split()[0]
    print(f'Processing {file_name}')

    # Only get species from blast hits that produced alignments
    if blast.alignments:
        alignment = blast.alignments[0]

        species = alignment.hit_def
        species_id = species.split()[0]

        # Percent identity of best hsp
        align_len = alignment.hsps[0].align_length
        no_identities = alignment.hsps[0].identities
        perc_id = no_identities / align_len
    else:
        species = perc_id = species_id = align_len = None

    return (file_name, read_id, blast.query_length, species, perc_id,
            align_len, species_id)


def main():
    parser = argparse.ArgumentParser(description="""Create csv with file names 
    and species they belong to from xml blast output""")
    parser.add_argument('--in-path', help='Path to directory with blast '
                                          'xml output files',
                        required=True)
    parser.add_argument('--out-path', help='Path to save the csv',
                        required=True)
    parser.add_argument('--num-workers', help='Multiprocessing cores to use',
                        required=True, type=int)
    args = parser.parse_args()

    all_files = Path(args.in_path).iterdir()

    # # Debugging thing here (uncomment to only look at 500 files)
    # all_files = [next(all_files) for _ in range(500)]
    with multiprocessing.Pool(args.num_workers) as p:
        ground_truths = p.map(parse_blast_xml, all_files)
    df = pd.DataFrame.from_records(ground_truths, columns=['file name',
                                                           'read id',
                                                           'query length',
                                                           'species',
                                                           'percent identity',
                                                           'alignment length',
                                                           'species id'])
    # Dirty fix because some species ID belong to same species
    df.loc[df['species id'] == 'NZ_DS264585.1', 'species id'] = 'NZ_DS264586.1'
    df.loc[df['species id'] == 'CP000143.2', 'species id'] = 'CP000144.2'
    df.to_csv(args.out_path, index=False)


if __name__ == '__main__':
    main()
