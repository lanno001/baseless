"""
This script was used to separate the initial 10% of data that we used for
hyperparameter optimisation and split that subset into train and test data
"""

import pandas as pd
from sklearn.model_selection import train_test_split
import argparse
import shutil
import os
from pathlib import Path
import multiprocessing


def move_files_from_list(file_list, in_directory, out_directory):
    """Given a list of file names and the directory they are in, move them to
    the out_directory

    :param file_list: list of filenames
    :type file_list: list[str]
    :param in_directory: path to directory that contains files from file_list
    :param out_directory: path to output directory
    """
    os.makedirs(out_directory, exist_ok=True)
    for i, file in enumerate(file_list):
        print(f"Moving {i / len(file_list) * 100:.1f}% complete")
        fast5_path = os.path.join(in_directory, file)
        shutil.move(fast5_path, out_directory)


def copy_files_from_list_parallel(file_list, in_directory, out_directory):
    """Given a list of file names and the directory they are in, copy them to
    the out_directory, all done in parallel.

    :param file_list: list of filenames
    :type file_list: list[str]
    :param in_directory: path to directory that contains files from file_list
    :param out_directory: path to output directory
    """
    os.makedirs(out_directory, exist_ok=True)
    # Generate paths to all files in the input directory
    file_paths = [os.path.join(in_directory, file) for file in file_list]

    # Prepare arguments for multiprocessing
    args = [[file_path, out_directory] for file_path in file_paths]
    # 60 core multiprocessing
    with multiprocessing.Pool(60) as p:
        p.starmap(shutil.copy, args)


def main():
    parser = argparse.ArgumentParser(description='Create directory with '
                                                 'train and test reads')
    parser.add_argument('--in-dir', help='Path to directory with fast5 files',
                        required=True)
    parser.add_argument('--out-dir',
                        help='Path in which to create train and test '
                             'directories',
                        required=True)
    parser.add_argument('--ground-truth',
                        help='Path to csv with ground truth labels, '
                             'to be used for stratified sampling. '
                             'Should contain the column \'species id\'.'
                             'It is output by set_ground_truths_of_reads.py',
                        required=True)
    parser.add_argument('--test-size',
                        help='Relative size of test dataset',
                        required=True, type=float, default=0.3)
    args = parser.parse_args()
    df = pd.read_csv(args.ground_truth)
    df.dropna(inplace=True)

    # Keep only files that are in the directory (not all ground truth files)
    files_in_dir = Path(args.in_dir).iterdir()
    filenames_in_dir = [file.name for file in files_in_dir]
    df = df[df['file name'].isin(filenames_in_dir)]

    # Split up the data
    train_data, test_data = train_test_split(df, test_size=args.test_size,
                                             stratify=df['species id'])
    train_files = list(train_data['file name'])
    test_files = list(test_data['file name'])

    move_files_from_list(train_files, in_directory=args.in_dir,
                         out_directory=os.path.join(args.out_dir, 'train'))
    move_files_from_list(test_files, in_directory=args.in_dir,
                         out_directory=os.path.join(args.out_dir, 'test'))


if __name__ == '__main__':
    main()
