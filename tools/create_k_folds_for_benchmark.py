"""
Given inital directory that contains all files that we use for model
benchmarking. Output folders in the following format to be used for k-fold CV
to benchmark Guppy, UNCALLED, SquiggleNet and DeepNano

demultiplexed_reads/
├── fast5/
│   ├── some_read1.fast5
│   ├── some_read2.fast5
│   ├── some_read3.fast5
│   ├── some_read4.fast5
│   └── some_read5.fast5
├── fold1/
│   ├── test/
│   │   ├── some_read4.fast5
│   │   └── some_read5.fast5
│   └── train_reads.txt -> contains 'some_read{1,2,3}.fast5'
├── fold2/
│   ├── test/
│   │   ├── some_read1.fast5
│   │   └── some_read2.fast5
│   └── train_reads.txt -> contains 'some_read{3,4,5}.fast5'

"""

import argparse
import pandas as pd
from sklearn.model_selection import StratifiedKFold
from pathlib import Path
import numpy as np

from tools.split_data_in_train_test import copy_files_from_list_parallel


def main():
    parser = argparse.ArgumentParser(description='Create folders to be used'
                                                 'for K-fold CV benchmarks. '
                                                 'Right now k=5 is hardcoded')
    parser.add_argument('--in-dir', help='Path to directory with all fast5 files',
                        required=True, type=Path)
    parser.add_argument('--out-base-dir',
                        help='Path in which to the create the fold directories',
                        required=True, type=Path)
    parser.add_argument('--ground-truth',
                        help='Path to csv with ground truth labels, '
                             'to be used for stratified sampling. '
                             'Should contain the column \'species id\'.'
                             'It is output by set_ground_truths_of_reads.py',
                        required=True, type=Path)
    args = parser.parse_args()

    df = pd.read_csv(args.ground_truth)
    df.dropna(inplace=True)

    # Keep only files that are in the directory (not all ground truth files)
    files_in_dir = args.in_dir.iterdir()
    filenames_in_dir = [file.name for file in files_in_dir]
    df = df[df['file name'].isin(filenames_in_dir)]

    skf = StratifiedKFold(n_splits=5, shuffle=True)
    files_to_split = df['file name'].to_numpy()
    file_labels = df['species'].to_numpy()

    for i, (train_idx, test_idx) in enumerate(skf.split(files_to_split,
                                                        file_labels)):
        print(f'Split {i}')
        # Save all training file names to txt file
        out_dir = args.out_base_dir / f'fold{i}'
        out_dir.mkdir()
        train_files = files_to_split[train_idx]
        print(f'Writing {len(train_files)} train files to txt')
        with open(out_dir / 'train_reads.txt', 'w+') as f:
            np.savetxt(f, train_files, fmt='%s')

        # Copy all test files to fold{i}/test/
        out_test_dir = out_dir / 'test'
        out_test_dir.mkdir()
        print(f'Copying test files to {out_test_dir}')
        test_files = files_to_split[test_idx]
        copy_files_from_list_parallel(test_files, args.in_dir, out_test_dir)


if __name__ == '__main__':
    main()
