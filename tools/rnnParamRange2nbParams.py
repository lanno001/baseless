import argparse, yaml

parser = argparse.ArgumentParser(description='return maximum number of parameters in network for given param range,'
                                             'only works for RNN configs!')
parser.add_argument('parameter_ranges_file', type=str)
args = parser.parse_args()

with open(args.parameter_ranges_file, 'r') as fh: ranges_dict = yaml.load(fh, yaml.FullLoader)

nl, ls = (ranges_dict['variable']['num_layers']['max'],
                   ranges_dict['variable']['layer_size']['max'])
max_nb_params = (((ls + 1) * ls + ls * 2) * 3  # first layer of GRUs
                     + (ls * 2 * ls + ls * 2) * 3 * nl - 1 # second to last layer of GRUs
                     + ls + 1) # fully connected last layer
ranges_dict['max_nb_params'] = max_nb_params
with open(args.parameter_ranges_file, 'w') as fh:
    yaml.dump(ranges_dict, fh)
