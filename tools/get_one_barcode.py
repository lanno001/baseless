import itertools
import multiprocessing
import os
from pathlib import Path
import pandas as pd
from Bio import SeqIO
import argparse
import shutil

# Never truncate strings:
pd.set_option('display.max_colwidth', None)


def copy_barcoded_fast5_reads(barcode_id, sequencing_summary_path, sequencing_dir,
                              out_dir, worker_count):
    """Copy all fast5 files that belong to a certain barcode to out_dir

    :param barcode_id: barcode to extract, example 'barcode12'
    :type barcode_id: str
    :param sequencing_summary_path: path to sequencing_summary.txt
    :param sequencing_dir: path to directory containing fast5 reads
    :param out_dir: where to save fast5 files
    :param worker_count: how many processes to run in parallel
    :return: list of fast5 filenames
    """
    # Index all files
    all_files = os.walk(sequencing_dir)

    barcode_df = summary_df_one_barcode(barcode_id, sequencing_summary_path)
    filename_of_barcoded_reads = barcode_df['filename'].squeeze().to_list()
    print('Indexed everything')

    # Prepare the arguments for the multiprocessing
    args = [[file_location, filename_of_barcoded_reads]
            for file_location in all_files]
    # Get all files in parallel
    with multiprocessing.Pool(worker_count) as p:
        files_of_read = p.starmap(find_reads_in_dir, args)
    # Merge list of lists into one big list
    files_of_read = itertools.chain.from_iterable(files_of_read)

    os.makedirs(out_dir, exist_ok=True)
    for file in files_of_read:
        shutil.copy(file, out_dir)

    return files_of_read


def fastq_to_fasta(fastq_filepath, df, outdir):
    """Convert single fastq file to fasta file and write to file in outdir

    :param fastq_filepath: string to single fastq file
    :param df: dataframe of sequencing_summary.txt
    :param outdir: directory to write fasta files to

    :return:
    """
    seqs = SeqIO.parse(fastq_filepath, 'fastq')
    for seq in seqs:
        # Get original filename from sequencing_summary using the sequence id
        filename = df.loc[df['read_id'] == seq.id]['filename']
        filename = filename.to_string(header=False, index=False)
        # Convert '.fastq' to '.fasta' here
        filename = filename[:-1] + 'a'
        print(f'Saving {filename}')
        SeqIO.write(seq, os.path.join(outdir, filename), 'fasta')


def get_barcode_fastq_reads(barcode_id, sequencing_summary_path,
                            sequencing_dir, out_dir, worker_count):
    """Wrapper function to get all fastq reads and convert them to fasta files
    with file names that correspond to the original fast5 files.

    :param barcode_id: barcode to extract, example 'barcode12'
    :type barcode_id: str
    :param sequencing_summary_path: path to sequencing_summary.txt
    :param sequencing_dir: path to directory containing fastq reads
    :param out_dir: where to save output fasta files
    :param worker_count: how many processes to run in parallel
    :return:
    """
    all_files = Path(sequencing_dir).iterdir()

    barcode_df = summary_df_one_barcode(barcode_id, sequencing_summary_path)

    # Prepare the arguments for the multiprocessing
    args = [[file, barcode_df, out_dir] for file in all_files]
    # Get all files in parallel
    with multiprocessing.Pool(worker_count) as p:
        p.starmap(fastq_to_fasta, args)


def summary_df_one_barcode(barcode_id, sequencing_summary_path):
    """Load sequencing summary and return dataframe with only
    reads of one barcode

    :param barcode_id: barcode to extract, example 'barcode12'
    :type barcode_id: str
    :param sequencing_summary_path: path to sequencing_summary.txt
    :return: dataframe
    """
    # Load all reads and their info from sequencing summary
    df = pd.read_csv(sequencing_summary_path, sep='\t',
                     usecols=['filename', 'read_id', 'run_id', 'batch_id',
                              'barcode_arrangement'])
    # Extract only reads that are of the target barcode
    filtered_df = df[df['barcode_arrangement'] == barcode_id]
    return filtered_df


def find_reads_in_dir(directory_tuple, id_of_barcoded_reads):
    """Given a certain directory tuple and list of valid files, return only the
    files in the directory that are valid, i.e. contain the barcode

    This is used for directories where not all reads belong to a barcode

    :param directory_tuple: tuple of (dirpath, dirnames, filenames)
                            output by os.walk
    :type directory_tuple: tuple
    :param id_of_barcoded_reads: list of all valid read ids
    :type id_of_barcoded_reads: list
    :return: list of file paths
    """
    files_of_read = []
    directory, _, file_names = directory_tuple
    files_of_read.extend([os.path.join(directory, file)
                          for file in file_names
                          if file in id_of_barcoded_reads])
    print(directory)
    print(len(files_of_read))
    return files_of_read


def main():
    parser = argparse.ArgumentParser(description="""Script to extract only
     certain files from a multiplex sample, created as a quicker alternative 
     to ont-api demultiplex script""")
    parser.add_argument('--seq-summary-path',
                        help='Path to sequencing_summary.txt', required=True)
    parser.add_argument('--read-path', help='Path to directory of reads',
                        required=True)
    parser.add_argument('--out-path',
                        help='Path to where output should be stored',
                        required=True)
    parser.add_argument('--barcode', help='String of barcode to extract,'
                                        ' e.g. \'barcode12\'', required=True)
    parser.add_argument('--worker-count', help='Number of parallel processes',
                        type=int, required=True)
    parser.add_argument('--input-type', choices=['fast5', 'fastq'],
                        help='Type of inputs, should be fast5 or fastq',
                        required=True)
    args = parser.parse_args()
    barcode = args.barcode
    seq_summary_path = args.seq_summary_path
    read_path = args.read_path
    out_dir = args.out_path
    worker_count = args.worker_count
    if args.input_type == 'fastq':
        """This one is for testing the fastq extraction"""
        get_barcode_fastq_reads(barcode, seq_summary_path, read_path,
                                out_dir, worker_count)
    elif args.input_type == 'fast5':
        copy_barcoded_fast5_reads(barcode, seq_summary_path, read_path,
                                  out_dir, worker_count)


if __name__ == '__main__':
    main()
