import argparse
import sys
from Bio import SeqIO
from pathlib import Path


def read_all_genbank_in_folder(file_path, out_path):
    """Given a directory of gbff files, extract all regions annotated as
    16s rRNA and write them to one fasta file in output directory

    :param file_path: path to directory
    :type file_path: str
    :param out_path: path to where output file should be saved
    :type out_path: str
    :return: None
    """
    all_files = Path(file_path).glob('**/*.gbff')
    all_fasta = []
    for file in all_files:
        all_fasta.extend(extract_16s_from_genbank(file))
    SeqIO.write(all_fasta, out_path, 'fasta')


def extract_16s_from_genbank(file):
    """From single genbank, return list of all 16s annotated regions

    :param file: path to single genbank file
    :return: list of 16s regions as SeqRecord objects
    """
    all_16s_regions = []
    genbank_file = SeqIO.parse(file, 'genbank')
    for record in genbank_file.records:
        for i, feature in enumerate(record.features):
            # make sure feature is rRNA and is referring to 16s subunit
            # Ugly parsing due to inconsistent structure between genbank files
            if (feature.type == 'rRNA'
                    and ('16S' in feature.qualifiers.get('product', [''])[0]
                         or '16s' in feature.qualifiers.get('gene', [''])[0])):
                r_rna_seq = feature.extract(record)
                # Prevent id = <unknown id> in final object
                r_rna_seq.id = ''
                # Set description that will later be used as fasta header
                r_rna_seq.description = f'{record.description} (16s region, feature {i}, loc: {str(feature.location)})'
                print(f'Found {r_rna_seq.description}')
                all_16s_regions.append(r_rna_seq)
    return all_16s_regions


def main():
    parser = argparse.ArgumentParser(description="""Script to select all 16s 
    rRNA regions from genbank files and write them to a single fasta file""")
    parser.add_argument('--in-path', required=True,
                        help='Path to directory containing genbank files')
    parser.add_argument('--out-path', required=True,
                        help='Path to filename where output should be stored')
    args = parser.parse_args()
    read_all_genbank_in_folder(args.in_path, args.out_path)


if __name__ == '__main__':
    main()
