import argparse, re, os, sys

import numpy as np
import pandas as pd
import multiprocessing as mp

from datetime import datetime
from os.path import basename
from shutil import copy
from sklearn.model_selection import StratifiedShuffleSplit

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
baseless_location = os.path.realpath(f'{__location__}/..')
sys.path.append(baseless_location)

from low_requirement_helper_functions import parse_output_path, parse_input_path


def parse_fast5_list(fast5_list, gt_df, focus_species, out_queue):
    fast5_basename_list = [basename(f) for f in fast5_list]
    species_list = [gt_df.species_short.get(ff, 'unknown') for ff in fast5_basename_list]
    if focus_species:
        species_list = [focus_species if sl == focus_species else 'other' for sl in species_list]
    fast5_df = pd.DataFrame({'read_id': fast5_basename_list,
                             'fn': fast5_list,
                             'species': species_list}
                            ).set_index('read_id')
    fast5_df.drop(fast5_df.query('species == "unknown"').index, axis=0, inplace=True)
    if focus_species:
        nb_min = int(fast5_df.groupby('species').nunique().min())
        fast5_df = pd.concat([ff.sample(nb_min) for _, ff in fast5_df.groupby('species')])
    out_queue.put(fast5_df)


def write_reads(fast5_sub_df, read_dir):
    for _, fn in fast5_sub_df.fn.iteritems():
        copy(fn, f'{read_dir}/')


parser = argparse.ArgumentParser(description='Take stratified test/train samples of defined size.')
parser.add_argument('--ground-truth', type=str, required=True,
                    help='csv denoting which species reads belong to')
parser.add_argument('--fast5-dir', type=str, required=True,
                    help='fast5 directory from which to take reads')
parser.add_argument('--out-dir', type=str, required=True)
parser.add_argument('--sample-size', type=int, default=1000,
                    help='Max number of reads to take (test + train) [default: 1000]')
parser.add_argument('--split', type=float, default=0.8,
                    help='Train/test split [default: 0.8]')
parser.add_argument('--focus-species', type=str, required=False,
                    help='Do not stratify, but 50/50 for a certain species')
parser.add_argument('--write-new-truth', action='store_true',
                    help='Parse out ground truth file to new ground truth table containing only included reads.')
parser.add_argument('--cores', type=int, default=4,
                    help='Number of cores to use, minimum 2 [default: 4]')
args = parser.parse_args()

# --- prep dirs ---
out_dir = parse_output_path(args.out_dir, clean=True)
train_dir = parse_output_path(out_dir + 'train')
test_dir = parse_output_path(out_dir + 'test')

# --- load ground truth ---
gt_df = pd.read_csv(args.ground_truth, header=0)
gt_df.columns = [cn.replace(' ', '_') for cn in gt_df.columns]
gt_df.set_index('file_name', inplace=True)

# --- index fast5 files ---
print(f'{datetime.now()}: parsing fast5 file list...')
fast5_list = np.array(parse_input_path(args.fast5_dir, pattern='*.fast5'))
nb_fast5 = len(fast5_list)
ff_idx_list = np.array_split(np.arange(len(fast5_list)), args.cores)
out_queue = mp.Queue()
ff_workers = [mp.Process(target=parse_fast5_list, args=(fast5_list[ff_idx], gt_df, args.focus_species, out_queue)) for ff_idx in ff_idx_list]
for worker in ff_workers: tst = worker.start()
df_list = []
while any(p.is_alive() for p in ff_workers):
    while not out_queue.empty():
        df_list.append(out_queue.get())
fast5_df = pd.concat(df_list)
print(f'{datetime.now()}: done')

# --- generate split ---
# species_list = list(fast5_df.species.unique())
sample_size = min(args.sample_size, len(fast5_df))
train_size = round(sample_size * args.split)
test_size = sample_size - train_size
train_num_idx, test_num_idx = tuple(StratifiedShuffleSplit(n_splits=1, train_size=train_size, test_size=test_size).split(fast5_df.index, fast5_df.species))[0]
train_idx, test_idx = fast5_df.index[train_num_idx], fast5_df.index[test_num_idx]
workers = [mp.Process(target=write_reads, args=(fast5_df.loc[idx, :], cdir))
           for idx, cdir in ((train_idx, train_dir), (test_idx, test_dir))]
for worker in workers:
    worker.start()
for worker in workers:
    worker.join()

if args.write_new_truth:
    test_files = [basename(ff) for ff in parse_input_path(test_dir)]
    gt_df_test = gt_df[np.in1d(gt_df.index, test_files)].to_csv(f'{out_dir}ground_truth_test.csv')
    train_files = [basename(ff) for ff in parse_input_path(train_dir)]
    gt_df_train = gt_df[np.in1d(gt_df.index, train_files)].to_csv(f'{out_dir}ground_truth_train.csv')

cp=1
