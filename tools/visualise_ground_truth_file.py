import argparse
from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


def main(args):
    id_to_species = {
        'NZ_CP053098': 'acinetobacter baumannii',
        'NZ_DS264586': 'actinomyces odontolyticus',
        # 'NZ_DS26458': 'schaalia odontolytica',
        'AE017194': 'bacillus cereus',
        'CP000139': 'bacteroides vulgatus',
        'CP000721': 'clostridium beijerinckii',
        'AE000513': 'deinococcus radiodurans',
        'CP025020': 'enterococcus faecalis',
        'U00096': 'escherichia coli',
        'AE000511': 'helicobacter pylori',
        'CP000413': 'lactobacillus gasseri',
        'AL591824': 'listeria monocytogenes',
        'AE002098': 'neisseria meningitides',
        'AP009380': 'porphyromonas gingivalis',
        'AE017283': 'propionibacterium acnes',
        'AE004091': 'pseudomonas aeruginosa',
        'CP000144': 'rhodobacter sphaeroides',
        'CP000730': 'staphylococcus aureus',
        'AE015929': 'staphylococcus epidermidis',
        'AE009948': 'streptococcus agalactiae',
        'AE014133': 'streptococcus mutans',
        'AE005672': 'streptococcus pneumoniae',
    }
    df = pd.read_csv(args.ground_truth)
    fig, axs = plt.subplots(2, 2, figsize=(10, 10))
    df['query length'].plot.hist(title='Query length', ax=axs[0, 0],
                                 bins=np.linspace(0, 3000, 50))

    percent_in_alignment = df['alignment length'] / df['query length']
    percent_in_alignment.plot.hist(title='Relative length of alignment relative to query',
                                   ax=axs[0, 1], bins=np.linspace(0.5, 1, 20))

    df['percent identity'].plot.hist(title='Percent identity (based on alignment length)',
                                     ax=axs[1, 0], bins=np.linspace(0.8, 1, 20))

    species_abundance = df['species id'].value_counts()
    species_abundance.index = [id_to_species[id[:-2]]
                               for id in species_abundance.index]
    species_abundance.plot.bar(title='Species abundance in ground truth',
                               ax=axs[1, 1])
    plt.setp(axs[1, 1].get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    fig.tight_layout()
    plt.savefig(args.out_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="""This script takes path to
        ground truth with alignment data and creates figure that shows read
         lengths, relative length of alignment, percent identity 
         based on alignment length and species abundance""")
    parser.add_argument('--ground-truth',
                        help='Path to csv with ground truth labels. '
                             'It is output by set_ground_truths_of_reads.py',
                        required=True, type=Path)
    parser.add_argument('--out-path',
                        help='Path to output figure',
                        required=True, type=Path)
    args = parser.parse_args()
    main(args)
