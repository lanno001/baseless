import pysam
import csv
import argparse
from low_requirement_helper_functions import parse_output_path

parser = argparse.ArgumentParser(description='Filter a directory of fast5 files, based on their presence in a bam file'
                                             ' or a selected region within that bam file. Return a list of present'
                                             ' files.')
parser.add_argument('-b', '--bam-file', type=str, required=True,
                    help='Bam file that will be used for read selection. Idex should be available at same location.')
parser.add_argument('--name-list', type=str, required=True,
                    help='tab-separated list containing read IDs and read names.')
parser.add_argument('-o', '--out-dir', type=str, required=True,
                    help='Directory where products will be stored')
parser.add_argument('-r', '--region', type=str,
                    help='Define a region which should be covered for at least half by the reads. Format should be'
                         ' contig,start,end without spaces.')
args = parser.parse_args()
bf = pysam.AlignmentFile(args.bam_file, 'rb')

args.out_dir = parse_output_path(args.out_dir)

# Read selection
if args.region:
    contig, start, end = args.region.split(',')
    start = int(start); end = int(end)
    fragment_length = end - start
else:
    contig = None; start = None; end = None
    fragment_length = 0
reads_iter = bf.fetch(contig, start, end)

read_id_list = []
for read in reads_iter:
    cur_read_list = str(read).split('\t')
    if int(cur_read_list[8]) > 0.5 * fragment_length:
        read_id_list.append(cur_read_list[0])

# Read ID to read name conversion
readname_dict = {}
with open(args.name_list, 'r') as rnf:
    tsvin = csv.reader(rnf, delimiter=' ')
    for nmp in tsvin:
        readname_dict[nmp[0]] = nmp[1]

read_list = []
unfound_list = []
with open(args.out_dir+'found_reads.txt', 'w') as rl, open(args.out_dir+'found_reads.txt', 'w') as ul:
    for rid in read_id_list:
        cur_rn = readname_dict.get(rid)
        if cur_rn:
            rl.write(cur_rn+'\n')
        else:
            ul.write(rid+'\n')
