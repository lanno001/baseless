import sys
import h5py
from db_building.TrainingRead import Read


class NNPrediction:
    """Class to predict if a target is in a read with pretrained neural networks

    :param kmer_lib_path: path to txt file containing kmers # TODO perhaps just extract these from the folder names?
    :type kmer_lib_path: str
    :param target: target sequence that BaseLess should search for
    :type target: str
    :param read_path: path to file containing read # TODO change this into multiple reads?
    :type read_path: str
    :param filter_width: width in which to cut the read before we predict it from its inference
    :type filter_width: int
    """

    def __init__(self, kmer_lib_path, target, read_path, **kwargs):
        self.target = target
        self.filter_width = kwargs['filter_width']

        self.read = self._split_up_read(read_path)
        self.target_kmers = self._which_kmers_in_target(kmer_lib_path)

    def _which_kmers_in_target(self, kmer_lib):
        """Get which kmers of the library are present in the target sequence

        :param kmer_lib: path to list of all kmers
        :type kmer_lib: str
        :return: list of kmers that are both in the library and the target seq
        """

        target_kmers = []
        with open(kmer_lib, 'r') as f:
            for kmer in f:
                kmer = kmer.strip()
                if kmer in self.target:
                    target_kmers.append(kmer)
        return target_kmers

    def _split_up_read(self, read_path):
        """

        :param read_path:
        :return:
        """
        with h5py.File(read_path, 'r') as f:
            return Read(f, normalization='median').get_split_raw_read(self.filter_width)

    def _scan_for_kmer(self, kmer):
        # Should point to the model path with the weights here
        model_path = ...
        # Load the neural network that belongs to that kmer here
        nn = ...
        # Do some preprocessing on self.read I suppose, like cutting it up
        prediction = nn.predict(self.read)
        return False  # Make this function just return a bool?

    def scan_for_target(self):
        found_kmers = [self._scan_for_kmer(kmer) for kmer in self.target_kmers]



def main():
    path_to_kmers = sys.argv[1]
    path_to_read = sys.argv[2]
    predictor = NNPrediction(path_to_kmers, 'ACCCTTCACAGATAGGAAA',
                             path_to_read, filter_width=1000)


if __name__ == "__main__":
    main()
