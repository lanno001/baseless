import os, re, yaml, subprocess, warnings, h5py, pickle

from pathlib import Path
from tempfile import TemporaryDirectory

import pandas as pd
import tensorflow as tf
import tensorflow.keras.backend as K
import numpy as np

from run_production_pipeline import main as rpp
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

COMPLEMENT_DICT = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C'}
kmer_freqs_df = pd.read_parquet(f'{__location__}/../data/ncbi_16S_bacteria_archaea_kmer_counts.parquet')
kmer_freqs_dict = dict(kmer_freqs_df.sum(axis=0))
# with open(f'{__location__}/../data/list_of_kmers_after_19_rounds.csv', 'r') as fh:
#     kmer_good_candidate_list = [km.strip() for km in fh.readlines() if len(km.strip())]
with open(f'{__location__}/../data/intermediate_9mers.csv', 'r') as fh:
    kmer_good_candidate_list = [km.strip() for km in fh.readlines() if len(km.strip())]


def reverse_complement(km):
    return ''.join([COMPLEMENT_DICT[k] for k in km][::-1])


def fa2kmers(fa_fn, kmer_size):
    """
    take multifasta file, return dict of lists of k-mers present in each sequence
    """
    with open(fa_fn, 'r') as fh:
        targets_txt = fh.read()
    targets_list = ['>' + t for t in targets_txt.split('>') if t]
    targets_dict = {re.search('(?<=>)[^\n]+', t).group(0): t for t in targets_list}
    kmer_dict = {}
    with TemporaryDirectory() as tdo:
        for td in targets_dict:
            with open(tdo + '/target.fasta', 'w') as fh:
                fh.write(targets_dict[td])
            subprocess.run(
                f'jellyfish count -m {kmer_size} -s {4 ** kmer_size} -C -o {tdo}/mer_counts.jf  {tdo}/target.fasta',
                shell=True)
            kmer_dump = subprocess.run(f'jellyfish dump -c {tdo}/mer_counts.jf', shell=True, capture_output=True)
            kmer_list = [km.split(' ')[0] for km in kmer_dump.stdout.decode('utf-8').split('\n')]
            kmer_list = [km for km in kmer_list if len(km) == kmer_size and km in kmer_good_candidate_list]  # retain k-mers found to score well in discernability
            kmer_list_curated = []  # remove reverse complements
            for km in kmer_list:
                if reverse_complement(km) in kmer_list_curated: continue
                kmer_list_curated.append(km)
            kmer_dict[td] = kmer_list_curated
    return kmer_dict


def get_kmer_candidates_16S(kmer_candidates_dict, min_nb_kmers=5, threshold=0.0001, filter_list=None):
    """
    get smallest set of 16S k-mers for which FDR<threshold
    """
    kmer_candidates_selection_dict = {}
    for kc in kmer_candidates_dict:
        p = 1.0
        kmer_list = sorted(kmer_candidates_dict[kc], key=kmer_freqs_dict.get, reverse=True)  # sort by frequency in 16S genomes
        kmer_candidates_selection_dict[kc] = kmer_list
        # kmer_candidates_selection_dict[kc] = []
        # sub_df = kmer_freqs_df.copy()
        # sub_freqs_dict = copy(kmer_freqs_dict)
        # cc=0
        # while cc <= len(kmer_list) and (p >= threshold or len(kmer_candidates_selection_dict[kc]) < min_nb_kmers):
        #     km = kmer_list.pop()  # pop least frequent k-mer
        #     kmer_candidates_selection_dict[kc].append(km)
        #
        #     # Select entries positive for k-mer or its reverse-complement
        #     km_rc = reverse_complement(km)
        #     hit_found = False
        #     if not len(sub_df):  # avoid making new queries if number of accessions is already 0, but min number of models has not been reached
        #         hit_found=True
        #     elif km in sub_df.columns and km_rc in sub_df.columns:
        #         sub_df = sub_df.query(f'{km} or {km_rc}').copy()
        #         hit_found=True
        #     elif km in sub_df.columns:
        #         sub_df.query(km, inplace=True)
        #         hit_found = True
        #     elif km_rc in sub_df.columns:
        #         sub_df.query(km_rc, inplace=True)
        #         hit_found = True
        #     if hit_found:
        #         hits = len(sub_df)
        #     else:
        #         hits = 1
        #     hits = max(hits, 1)
        #     p *= (hits - 1) / hits  # update FDR
        #     kmer_list.sort(key=sub_freqs_dict.get, reverse=True)  # re-sort kmer list
        #     sub_freqs_dict = dict(sub_df.sum(axis=0))
        #     cc += 1
        if filter_list:
            kmer_candidates_selection_dict[kc] = [km for km in kmer_candidates_selection_dict[kc] if km in filter_list]
    kc_count_dict = {kc: len(kmer_candidates_selection_dict[kc]) for kc in kmer_candidates_selection_dict}
    selected_id = min(kc_count_dict, key=kc_count_dict.get)
    return kmer_candidates_selection_dict[selected_id]


def compile_model(kmer_dict, filter_width, filter_stride, threshold, k_frac):
    k_threshold = round(len(kmer_dict) * k_frac)

    input = tf.keras.Input(shape=(None, 1), ragged=True)
    input_strided = tf.signal.frame(input.to_tensor(default_value=np.nan), frame_length=filter_width, frame_step=filter_stride, axis=1)
    input_strided = tf.keras.layers.Masking(mask_value=np.nan)(input_strided)
    ht_list = []
    for km in kmer_dict:
        mod = tf.keras.models.load_model(f'{kmer_dict[km]}/nn.h5',compile=False)
        mod._name = km
        h = tf.keras.layers.TimeDistributed(mod)(input_strided)
        h = K.max(h, axis=1)
        ht_list.append(K.cast_to_floatx(K.greater(h, threshold)))
    nb_hits = tf.math.reduce_sum(tf.keras.layers.concatenate(ht_list), axis=1)
    output = K.greater(nb_hits, k_threshold)
    meta_mod = tf.keras.Model(inputs=input, outputs=output)
    meta_mod.compile()
    return meta_mod


def compile_model_abundance(kmer_dict, filter_width, filter_stride, threshold):
    input = tf.keras.Input(shape=(None, 1), ragged=True)
    input_strided = tf.signal.frame(input.to_tensor(default_value=np.nan), frame_length=filter_width, frame_step=filter_stride, axis=1)
    input_strided = tf.keras.layers.Masking(mask_value=np.nan)(input_strided)
    ht_list = []
    for km in kmer_dict:
        mod = tf.keras.models.load_model(f'{kmer_dict[km]}/nn.h5',compile=False)
        mod._name = km
        h = tf.keras.layers.TimeDistributed(mod)(input_strided)
        h = K.cast_to_floatx(K.greater(h, threshold))
        h = K.sum(h, axis=0)
        h = K.sum(h, axis=0)
        ht_list.append(h)
    output = tf.keras.layers.concatenate(ht_list)
    meta_mod = tf.keras.Model(inputs=input, outputs=output)
    meta_mod.compile()
    return meta_mod


def train_on_the_fly(kmer_list, available_mod_dict, args):
    kmers_no_models = [km for km in kmer_list if km not in available_mod_dict]
    if len(kmers_no_models):  # train additional models, if required
        print(f'No models found for {len(kmers_no_models)} kmers, training on the fly!')
        args.kmer_list = kmers_no_models
        rpp(args)
        # add newly generated models to available model list
        for km in kmers_no_models:
            if os.path.exists(f'{args.out_dir}nns/{km}/nn.h5'):  # Check to filter out failed models
                available_mod_dict[km] = f'{args.out_dir}nns/{km}'
            else:
                warnings.warn(
                    f'model generation failed for {km}, see {args.out_dir}logs. Continuing compilation without it.')
                kmer_list.remove(km)
    out_dict = {km: available_mod_dict.get(km, None) for km in kmer_list if km in available_mod_dict}
    return out_dict


def filter_accuracy(kmer_dict, acc_threshold):
    out_dict = {}
    discard_list = []
    for kmd in kmer_dict:
        perf_fn = kmer_dict[kmd] + '/performance.pkl'
        if not os.path.isfile(perf_fn): continue
        with open(perf_fn, 'rb') as fh: perf_dict = pickle.load(fh)
        # metric = 2 / ( perf_dict['val_precision'][-1] ** -1 + perf_dict['val_recall'][-1] ** -1)  # F1
        metric = perf_dict['val_binary_accuracy'][-1]  # plain accuracy
        if metric > acc_threshold:
            out_dict[kmd] = kmer_dict[kmd]
        else:
            discard_list.append(kmd)
    return out_dict, discard_list


def main(args):

    # List for which k-mers models are available
    if args.nn_directory:
        available_mod_dict = {pth.name: str(pth) for pth in Path(args.nn_directory).iterdir() if pth.is_dir()}
    elif args.target_16S:
        available_mod_dict = {pth.name: str(pth) for pth in Path(f'{__location__}/../16S_db/').iterdir() if pth.is_dir()}
    else:
        available_mod_dict = {}

    with open(args.parameter_file, 'r') as fh:
        param_dict = yaml.load(fh, yaml.FullLoader)

    # Parse target k-mers
    if args.kmer_list:  # parse a given list of kmers
        with open(args.kmer_list, 'r') as fh:
            requested_kmer_list = [km.strip() for km in fh.readlines()]
        requested_kmer_list = [km for km in requested_kmer_list if len(km)]
        # requested_kmer_list = [km for km in requested_kmer_list if km in kmer_good_candidate_list]
        # if not len(requested_kmer_list):
        #     raise ValueError('None of requested k-mers is considered well-discernible!')
        if args.train_required:
            target_kmer_dict = train_on_the_fly(requested_kmer_list, available_mod_dict, args)
        else:
            target_kmer_dict = {km: available_mod_dict.get(km, None) for km in requested_kmer_list if km in available_mod_dict}
    elif args.target_16S:  # estimate salient set of kmers from given 16S sequence
        requested_kmer_dict = fa2kmers(args.target_16S, param_dict['kmer_size'])  # Collect k-mers per sequence in target fasta marked as recognizable
        if args.train_required:
            target_kmer_list = get_kmer_candidates_16S(requested_kmer_dict, args.min_nb_models, 0.0001)
            target_kmer_dict = train_on_the_fly(target_kmer_list, available_mod_dict, args)
        else:  # filter out k-mers for which no stored model exists
            target_kmer_list = get_kmer_candidates_16S(requested_kmer_dict, args.min_nb_models, 0.0001, filter_list=list(available_mod_dict))
            target_kmer_dict = {km: available_mod_dict.get(km, None) for km in target_kmer_list if km in available_mod_dict}
        if not len(target_kmer_dict):
            raise ValueError('Sequences do not contain any of available models!')
    else:
        raise ValueError('Either provide --nn-directory or --target-16S')

    if args.accuracy_threshold:
        target_kmer_dict, discard_list = filter_accuracy(target_kmer_dict, args.accuracy_threshold)
        print(f'discarded {len(discard_list)} k-mers because accuracy is lower than threshold: {discard_list}')

    if args.model_type == 'binary':
        mod = compile_model(target_kmer_dict,
                            param_dict['filter_width'], param_dict['filter_stride'],
                            param_dict['threshold'], param_dict['k_frac'])
    elif args.model_type == 'abundance':
        mod = compile_model_abundance(target_kmer_dict,
                                      param_dict['filter_width'], param_dict['filter_stride'],
                                      param_dict['threshold'])
    else:
        raise ValueError(f'--model-type {args.model_type} not implemented')
    mod.save(args.out_model)
    with h5py.File(args.out_model, 'r+') as fh:
        fh.attrs['model_type'] = args.model_type
        fh.attrs['kmer_list'] = ','.join(list(target_kmer_dict))
