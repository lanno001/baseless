import sys, signal, os, h5py, shutil
from pathlib import Path
from datetime import datetime
import tensorflow as tf
from tensorflow.python.saved_model import tag_constants
import numpy as np

sys.path.append(f'{list(Path(__file__).resolve().parents)[1]}')
from low_requirement_helper_functions import parse_output_path
from inference.ReadTable import ReadTable
from argparse_dicts import get_run_inference_parser


def main(args):
    os.environ["OMP_NUM_THREADS"] = "1"
    os.environ["TF_NUM_INTRAOP_THREADS"] = "1"
    os.environ["TF_NUM_INTEROP_THREADS"] = "1"
    tf.config.threading.set_intra_op_parallelism_threads(1)
    tf.config.threading.set_inter_op_parallelism_threads(1)
    tf.config.set_soft_device_placement(True)

    gpus = tf.config.experimental.list_physical_devices('GPU')
    if args.mem > 0:
        tf.config.experimental.set_virtual_device_configuration(gpus[0], [
            tf.config.experimental.VirtualDeviceConfiguration(memory_limit=args.mem)])
    # if gpus:
    #     try:
    #         # Currently, memory growth needs to be the same across GPUs
    #         for gpu in gpus:
    #             tf.config.experimental.set_memory_growth(gpu, True)
    #         logical_gpus = tf.config.list_logical_devices('GPU')
    #         print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    #     except RuntimeError as e:
    #         # Memory growth must be set before GPUs have been initialized
    #         print(e)

    print('Loading model...')

    if args.model.endswith('.h5'):
        mod = tf.keras.models.load_model(args.model)
        with h5py.File(args.model, 'r') as fh:
            model_type = fh.attrs['model_type']
            kmer_list = fh.attrs['kmer_list'].split(',')
    else:  # assume its a trt model
        mod_dir = args.model
        if not mod_dir.endswith('/'): mod_dir += '/'
        _mod = tf.saved_model.load(args.model, tags=[tag_constants.SERVING])
        mod = _mod.signatures['serving_default']
        with open(f'{mod_dir}baseless_params.txt', 'r') as fh:
            model_type, km = fh.read().split('\n')
            kmer_list = km.split(',')
    print(f'Done! Model type is {model_type}')
    pos_reads_dir = parse_output_path(args.out_dir + 'pos_reads')

    # for abundance estimation mode
    abundance_mode = False
    if model_type == 'abundance':
        abundance_array = np.zeros(len(kmer_list))
        abundance_mode = True

    # Load read table, start table manager
    read_table = ReadTable(args.fast5_in, pos_reads_dir)
    read_manager_process = read_table.init_table()

    # ensure processes are ended after exit
    def graceful_shutdown(sig, frame):
        print("shutting down")
        read_manager_process.terminate()
        read_manager_process.join()
        sys.exit(0)

    # Differentiate between inference modes by defining when loop stops
    if args.inference_mode == 'watch':
        signal.signal(signal.SIGINT, graceful_shutdown)
        def end_condition():
            return True
    elif args.inference_mode == 'once':
        def end_condition():
            return len(os.listdir(args.fast5_in)) != 0
    else:
        raise ValueError(f'{args.inference_mode} is not a valid inference mode')
    start_time = datetime.now()
    # Start inference loop
    while end_condition():
        read_id, read = read_table.get_read_to_predict(batch_size=args.batch_size)
        if read is None: continue
        pred = mod(read).numpy()
        if abundance_mode:
            abundance_array += pred
            read_table.update_prediction(read_id, np.zeros(len(read_id), dtype=bool))
        else:
            read_table.update_prediction(read_id, pred)
    else:
        run_time = datetime.now() - start_time
        read_manager_process.terminate()
        read_manager_process.join()
        with open(args.out_dir + 'run_stats.log', 'w') as fh:
            fh.write(f'wall_time: {run_time.seconds}s')
        print(f'wall time was {run_time.seconds} s')
        if abundance_mode:
            freq_array = abundance_array / max(abundance_array.sum(), 1)
            abundance_txt = 'kmer,abundance,frequency\n' + \
                            '\n'.join([f'{km},{ab},{fr}' for km, ab, fr in zip(kmer_list, abundance_array, freq_array)])
            with open(f'{args.out_dir}abundance_estimation.csv', 'w') as fh:
                fh.write(abundance_txt)

if __name__ == '__main__':
    parser = get_run_inference_parser()
    args = parser.parse_args()
    if args.copy_reads:
        fast5_in_new = parse_output_path(args.out_dir + 'fast5_in/', clean=True)
        shutil.copytree(args.fast5_in, fast5_in_new, dirs_exist_ok=True)
        args.fast5_in = fast5_in_new
    main(args)
