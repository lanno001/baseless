import sys, signal, os, h5py
from datetime import datetime
import pandas as pd
from low_requirement_helper_functions import parse_output_path
from inference.ReadTable import ReadTable
import tensorflow as tf
import numpy as np

def main(args):
    """
    Like run_inference, but with extra bells and whistles for tool evaluation
    """
    tf.config.threading.set_intra_op_parallelism_threads(1)
    tf.config.threading.set_inter_op_parallelism_threads(1)
    print('Loading model...')
    mod = tf.keras.models.load_model(args.model)
    with h5py.File(args.model, 'r') as fh:
        model_type = fh.attrs['model_type']
        kmer_list = fh.attrs['kmer_list'].split(',')
    print(f'Done! Model type is {model_type}')
    pos_reads_dir = parse_output_path(args.out_dir + 'pos_reads')

    # for abundance estimation mode
    abundance_mode = False
    if model_type == 'abundance':
        abundance_dict = {}
        nb_reads= 0
        abundance_mode = True

    # Load read table, start table manager
    read_table = ReadTable(args.fast5_in, pos_reads_dir)
    read_manager_process = read_table.init_table()

    # ensure processes are ended after exit
    def graceful_shutdown(sig, frame):
        print("shutting down")
        read_manager_process.terminate()
        read_manager_process.join()
        sys.exit(0)

    # Differentiate between inference modes by defining when loop stops
    if args.inference_mode == 'watch':
        signal.signal(signal.SIGINT, graceful_shutdown)
        def end_condition():
            return True
    elif args.inference_mode == 'once':
        def end_condition():
            return len(os.listdir(args.fast5_in)) != 0
    else:
        raise ValueError(f'{args.inference_mode} is not a valid inference mode')
    start_time = datetime.now()
    # Start inference loop
    while end_condition():
        read_id, read = read_table.get_read_to_predict()
        if read is None: continue
        pred = mod(read).numpy()
        if abundance_mode:
            nb_reads += len(read_id)
            abundance_dict[nb_reads] = pred
            read_table.update_prediction(read_id, np.zeros(len(read_id), dtype=bool))
        else:
            read_table.update_prediction(read_id, pred)
    else:
        run_time = datetime.now() - start_time
        read_manager_process.terminate()
        read_manager_process.join()
        print(f'rutime was {run_time.seconds} s')
        if abundance_mode:
            abundance_df = pd.DataFrame.from_dict(abundance_dict, orient='index', columns=kmer_list)
            abundance_df.to_csv(f'{args.out_dir}abundance_estimation.csv')
