"""
In this script we can count for the 16srRNA sequences from the following URL
ftp://ftp.ncbi.nlm.nih.gov/refseq/TargetedLoci/Bacteria/bacteria.16SrRNA.fna.gz
It requires jellyfish an installation of jellyfish to count the kmers.

I can also expand it later so it shows the ubiquity of the library of kmers we
created.
"""
import argparse
import itertools
import os
from Bio import SeqIO
import subprocess
import time
import json
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from k_mer_design.simulate_squiggles import generate_kmers


class KmerUbiquityCounter:
    """This class is used to count for each kmer in how many sequences in
    the fasta file it occurs at least once"""
    def __init__(self, k, fasta_seqs_path):
        self.kmer_counter_dict = {kmer: 0 for kmer in generate_kmers(k)}
        self.k = k
        self.fasta_seqs_path = fasta_seqs_path # Path to file with fasta sequences
        self.total_sequences = len(list(SeqIO.parse(self.fasta_seqs_path,
                                                    'fasta'))) # Total number of sequences

    def find_kmers_in_fasta(self):
        """Given a long list of fasta sequences, count for each kmer
           in how many of the sequences it occurs at least once"""
        hash_estimate = 4**self.k
        for sequence in SeqIO.parse(self.fasta_seqs_path, 'fasta'):
            print(sequence.id)
            fasta_seq = f">{sequence.id}\n{sequence.seq}\n"
            # Count the kmers and store in hash
            subprocess.run(f'jellyfish count -m {self.k} -s {hash_estimate} /dev/fd/0',
                           input=fasta_seq, shell=True, text=True)
            # Dump the hash into stdout
            kmer_dump = subprocess.run('jellyfish dump -c mer_counts.jf', shell=True, capture_output=True)
            output = kmer_dump.stdout.decode('utf-8').split('\n')
            for kmer_line in output:
                if kmer_line: # Skip empty lines
                    #print(kmer_line[:self.k])
                    self.kmer_counter_dict[kmer_line[:self.k]] += 1

        return self.kmer_counter_dict


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Count 8mers in sequences '
                                                 'of species, e.g. 16srRNA')
    parser.add_argument('--sequences', required=True,
                        help='Path to fasta file with sequences of species')
    parser.add_argument('-o', '--out-directory', required=True,
                        help='Output directory')
    args = parser.parse_args()

    if not os.path.exists(args.out_directory):
        os.mkdir(args.out_directory)

    tic = time.perf_counter()
    bacteria_counter = KmerUbiquityCounter(8, args.sequences)
    print(bacteria_counter.total_sequences)
    bacteria_kmer_ubiquity = bacteria_counter.find_kmers_in_fasta()
    toc = time.perf_counter()
    print(toc - tic)

    with open(os.path.join(args.out_directory, 'bacteria_counter_result.txt',
                           'w+')) as f:
        f.write(json.dumps(bacteria_kmer_ubiquity))

    with open(os.path.join(args.out_directory,'bacteria_counter_result.txt',
                           'r')) as f:
        f = f.read()
        bact_kmer_count_dict = json.loads(f)

    # Create dataframe and infer relative abundances
    df = pd.DataFrame.from_dict(bact_kmer_count_dict, orient='index',
                                columns=['Count'])
    df['Relative Abundance'] = df['Count'] / 20959

    outer_10 = df[(df['Relative Abundance'] > df[
        'Relative Abundance'].quantile(0.95))  # Select outer 10% of kmers
                  | (df['Relative Abundance'] < df[
        'Relative Abundance'].quantile(0.05))]

    with open(os.path.join(args.out_directory,'outer10kmers.json',
                           'w+')) as f:
        f.write(json.dumps(list(outer_10.index)))

