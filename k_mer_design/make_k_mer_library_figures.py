"""
Originally this was in a jupyter notebook but to keep everything in one place
I'll put the code here
"""
import argparse
from pathlib import Path

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import scipy.spatial as sp
import scipy.cluster.hierarchy as hc

def main():
    parser = argparse.ArgumentParser(description="""Create plots from 
    libraries of k-mers""")
    parser.add_argument('--dtw-file', required=True,
                        help='Path to k-mer dynamic time warping distance '
                             'matrix output by evolutionary k-mer algorithm',
                        type=Path)
    parser.add_argument('--sw-file', required=True,
                        help='Path to k-mer Smith-Waterman distance matrix '
                             'output by evolutionary k-mer algorithm',
                        type=Path)
    parser.add_argument('--out-dir',
                        help='Directory in which to create the output figures',
                        required=True, type=Path)
    args = parser.parse_args()
    args.out_dir.mkdir(exist_ok=True)
    final_df_dtw = pd.read_csv(args.dtw_file, index_col=0)
    final_df_sw = pd.read_csv(args.sw_file, index_col=0)
    current_seqs = list(final_df_dtw.index)

    # Save histogram of DTW and SW distances
    fig, ax = plt.subplots()
    # Get only distances in upper triangle
    upper_tri_dtw = final_df_dtw.to_numpy()[np.triu_indices(1500, k=1)].flatten()
    sns.histplot(upper_tri_dtw, ax=ax)
    ax.set_xlabel("DTW distance")
    ax.set_title('Pairwise distances of k-mers')
    fig.tight_layout()
    fig.savefig(args.out_dir / 'pairwise_DTW_k-mer_distances.png')

    fig, ax = plt.subplots()
    upper_tri_sw = final_df_sw.to_numpy()[np.triu_indices(1500, k=1)].flatten()
    sns.histplot(upper_tri_sw, ax=ax, discrete=True)
    ax.set_xlabel("SW distance")
    ax.set_title('Pairwise distances of k-mers')
    fig.tight_layout()
    fig.savefig(args.out_dir / 'pairwise_SW_k-mer_distances.png')

    for method in ['average', 'complete']:
        linkage = hc.linkage(sp.distance.squareform(final_df_dtw),
                             method=method)
        g = sns.clustermap(final_df_dtw, row_linkage=linkage,
                           col_linkage=linkage, cbar_pos=(.1, .815, .05, .18),
                           cbar_kws={'aspect': 15})

        g.fig.suptitle(f'{method.capitalize()} clustering of '
                       f'k-mers on DTW distance',
                       fontsize='xx-large')
        g.fig.tight_layout(pad=0.3)
        # Prevent square colourbar
        g.ax_cbar.set_aspect(4)
        g.savefig(args.out_dir / f'kmers_{method}_clustering.png')


if __name__ == '__main__':
    main()
